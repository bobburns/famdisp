package famdisp

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	"errors"
	"fmt"
	g "github.com/tncardoso/gocurses"
	session "gitlab.com/bobburns/famsess"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var BadMessErr = errors.New("Bad Message format")

type Message struct {
	From string
	Date int64
	File string
	Body []byte
	// index for text editing
	EndIndex int
	Y        []int
	X        []int
}

func ParseMessage(d string) (Message, error) {
	m := Message{
		From: "Unknown",
		Date: 0,
		File: "??",
		Body: []byte(""),
	}
	//fmt.Println(d)
	reader := strings.NewReader(d)
	scanner := bufio.NewScanner(reader)

	scanner.Scan()
	ftext := scanner.Text()
	//fmt.Println(ftext)
	fromarr := strings.Split(ftext, ":")
	if fromarr[0] != "From" || len(fromarr) < 2 {
		return m, BadMessErr
	}
	m.From = fromarr[1]

	//scan date
	scanner.Scan()
	dtext := scanner.Text()
	//fmt.Println(dtext)
	darr := strings.Split(dtext, ":")
	if darr[0] != "Date" || len(darr) < 2 {
		return m, BadMessErr
	}
	date, err := strconv.ParseInt(darr[1], 10, 64)
	if err != nil {
		return m, err
	}
	m.Date = date

	body := ""
	// scan the rest of message into body
	for scanner.Scan() {
		body += scanner.Text() + "\n"
	}

	if err := scanner.Err(); err != nil {
		return m, err
	}
	m.Body = []byte(body)
	return m, nil

}

type MessDisplay struct {
	Window   *g.Window
	Index    int
	Set      int
	Messages []Message
	Out      Message
	Rows     int
}

func (m *MessDisplay) ShowMessages() {
	var i int
	//clear main window
	r, _ := m.Window.Getmaxyx()
	for i = 0; i < r-1; i++ {
		m.Window.Mvaddstr(i, 0, "\n")
	}

	if len(m.Messages) == 0 {
		m.Window.Mvaddstr(0, 0, "No Messages to display")
		m.Window.Refresh()
		return
	}
	layout := "Jan _2, 2006 3:04PM"
	i = 0
	for j := m.Index; j > (m.Index - m.Set); j-- {
		if j < 0 {
			m.Window.Mvaddstr(i*6, 0, "\n\n\n\n\n\n")
			continue
		}
		head := fmt.Sprintf("<--------------- Message %3d ------------------>\n", j)
		from := fmt.Sprintf("From: %s Date: %s\n", m.Messages[j].From, time.Unix(m.Messages[j].Date, 0).Format(layout))
		m.Window.Mvaddstr(i*6, 0, head)
		m.Window.Addstr(from)
		m.Window.Addstr(string(m.Messages[j].Body))
		i++
	}
	m.Window.Refresh()
}
func (md *MessDisplay) GetText(w *g.Window, new bool) {
	w.Keypad(true)
	g.Noecho()
	btext := make([]byte, 512)
	x := make([]int, 512)
	y := make([]int, 512)
	//i := m.Out.CurIndex
	i, j := 0, 0
	my, mx := w.Getmaxyx()

	if new {
		w.Mvaddstr(1, 0, "\n\n\n\n")
		w.Refresh() // clear text
	}
	w.Mvaddch(1, 0, '@')
	// insert out string
	if !new {
		w.Mvaddstr(1, 1, string(md.Out.Body))
		copy(btext, md.Out.Body)
		j = md.Out.EndIndex
		copy(y, md.Out.Y)
		copy(x, md.Out.X)
		w.Wmove(y[i], x[i])
	}

	// get starting cursor position
	y[i], x[i] = w.Getyx()

	// keyin loop
	for {
		c := w.Getch()

		if c == 127 || c == g.KEY_DC || c == g.KEY_BACKSPACE {
			if i > 0 && i <= 200 {
				i--
				w.Wmove(y[i], x[i])
				w.Wdelch()
				btext = append(btext[:i], btext[i+1:]...)
			}
			continue
		}
		if c == '^' || c == 27 {
			break
		}
		// goto start
		if c == g.KEY_UP {
			i = 0
			w.Wmove(y[i], x[i])
			continue
		}
		// goto end
		if c == g.KEY_DOWN {
			i = j - 1
			w.Wmove(y[i], x[i])
			continue
		}
		if c == g.KEY_LEFT {
			if i > 0 {
				i--
				w.Wmove(y[i], x[i])
			}
		}
		if c == g.KEY_RIGHT {
			if i < (j - 1) {
				i++
				w.Wmove(y[i], x[i])
			}
		}

		if c > 31 && c < 127 || c == 10 || c == 13 {
			cy, cx := w.Getyx()
			if i < 200 && (cy <= my && cx < mx) {
				y[i], x[i] = w.Getyx()
				btext[i] = byte(c)
				w.Addch(c)
				// handle newline
				if i >= j {
					j++
				}
				i++
			}
		}
	}
	md.Out = Message{
		From:     "bla",
		Date:     time.Now().Unix(),
		Body:     btext[:],
		EndIndex: j,
		Y:        y[:],
		X:        x[:],
	}
	//w.Mvaddstr(4, 0, "Got Text!")
	//w.Mvaddstr(2, 0, m.Out.Body)
	w.Refresh()

}

func (md *MessDisplay) DisplayStr(s string) {
	row := md.Rows - 7

	md.Window.Mvaddstr(row, 0, "\n ")
	if s == "" {
		md.Window.Refresh()
		return
	}
	md.Window.Attron(g.A_REVERSE)
	md.Window.Mvaddstr(row, 0, s)
	md.Window.Attroff(g.A_REVERSE)
	md.Window.Refresh()
}

// erase text window
//func cleartext(w *g.Window) {
//
//	w.Mvaddstr(1, 0, "\n\n\n\n")
//	w.Refresh() // clear text
//}
func GetLine(w *g.Window) string {

	i := 0
	y, x := w.Getyx()
	btext := make([]byte, 24)

	// keyin loop
	for {
		c := w.Getch()

		if c == 10 || c == 13 || i > 23 {
			break
		}

		if c == 127 || c == g.KEY_DC || c == g.KEY_BACKSPACE {
			if i > 0 {
				i--
				x--
				w.Wmove(y, x)
				w.Wdelch()
				btext = append(btext[:i], btext[i+1:]...)
			}
			continue
		}

		btext[i] = byte(c)
		w.Addch(c)
		i++
	}
	return string(btext)
}
func (md *MessDisplay) Send(fams *session.Session) error {

	r, _ := regexp.Compile("^[[:alnum:]]+\\s")
	data := string(bytes.Trim(md.Out.Body, "\x00"))
	to := r.FindString(data)
	to = strings.TrimSpace(to)
	body := data[len(to):]

	//err := fams.RequestKey(to)
	//if err != nil {
	//	return err
	//}
	//send date as int64 string
	date := strconv.FormatInt(time.Now().Unix(), 10)
	body = "From:" + fams.Account + "\n" + "Date:" + date + "\n" + body

	fams.ToFrom = to
	encMess, err := fams.EncryptMessage(body)
	if err != nil {
		return err
	}

	err = fams.MessagePreSend(to)
	if err != nil {
		return err
	}
	err = fams.MessageSend(encMess)
	if err != nil {
		return err
	}
	//	os.Exit(0)

	return nil
}
func (md *MessDisplay) Delete(fams *session.Session) error {

	input := []byte{}
	i := 0
	var c int
	for {
		c = md.Window.Getch()
		if c == 'M' || i > 4 {
			break
		}
		if c >= '0' && c <= '9' {
			input = append(input, byte(c))
			i++
		}
	}
	val, err := strconv.Atoi(string(input))
	if err != nil {
		return errors.New("input error")
	}
	dstr := fmt.Sprintf("Delete message#%d ??", val)
	md.DisplayStr(dstr)
	c2 := md.Window.Getch()
	if c2 != 'y' {
		return errors.New("Cancled Delete")
	}
	if val >= len(md.Messages) {
		return errors.New("No Message to Delete")
	}

	md.DisplayStr("Deleting... ")
	err = fams.MessageDel(md.Messages[val].File)
	if err != nil {
		return err
	}
	return nil
}
func (md *MessDisplay) ChangePassword(fams *session.Session) error {
	dstr := fmt.Sprintf("Enter old Password: ")
	md.DisplayStr(dstr)
	old := md.getinput()
	dstr = fmt.Sprintf("Enter new Password: ")
	md.DisplayStr(dstr)
	new1 := md.getinput()
	dstr = fmt.Sprintf("Re-Enter new Password: ")
	md.DisplayStr(dstr)
	new2 := md.getinput()
	if !bytes.Equal(new1[:], new2[:]) {
		return errors.New("New Passwords don't match")
	}

	//convert to sha256 sum
	oldsha := fmt.Sprintf("%x", sha256.Sum256(old[:]))
	newsha := fmt.Sprintf("%x", sha256.Sum256(new2[:]))
	payload := oldsha + ":" + newsha
	err := fams.ClientChgPass(payload)
	if err != nil {
		return err
	}
	return nil

}

func (md *MessDisplay) getinput() []byte {
	input := []byte{}
	i := 0
	var c int
	for {
		c = md.Window.Getch()
		if c == 10 || c == 13 || i > 23 {
			break
		}
		if c >= '!' && c <= '~' {
			input = append(input, byte(c))
			i++
		}
	}
	return bytes.TrimSpace(input)
}
